# sketch-design-system-export
A simple Sketch plugin to extract details about a design system page and insert into JSON

Feel free to copy, clone, strip and restructure this as needed.
